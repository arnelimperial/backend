from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from backend.users.api.views import UserViewSet
from backend.mensahe.api.views import MessagesViewSet
from backend.blogs.api.views import BlogViewSet


if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("messages", MessagesViewSet)
router.register("blogs", BlogViewSet)



app_name = "api"
urlpatterns = router.urls
