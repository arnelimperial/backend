from rest_framework.permissions import BasePermission


class PostOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        if self.action in ('create',):
            return True
        return False
