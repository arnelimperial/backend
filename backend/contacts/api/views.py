from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from .serializers import ContactsSerializer
from .permissions import PostOnlyPermission
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import APIException
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

arnel = getattr(settings, 'EMAIL_RECEIVER')
default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')

@method_decorator(csrf_exempt, name='dispatch')
class ContactsAPIView(APIView):
    #permission_classes = (PostOnlyPermission,)
    permission_classes = (AllowAny,)
    
    def get(self, request, *args, **kwargs):
        serializer = ContactsSerializer()
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = ContactsSerializer(data=request.data)
      
        if serializer.is_valid():
            data = serializer.validated_data
            
            first_name = data.get('first_name')
            last_name = data.get('last_name')
            email = data.get('email')
            subject = data.get('subject')
            message = data.get('message')
            #attachment = data.get('attachement')

            html_content = render_to_string('contacts/copy.html', {
                'first_name':first_name, 
                'last_name':last_name, 
                'first_name':first_name, 
                'email':email, 
                'subject':subject,
                'message':message
                }
            )

            text_content = strip_tags(html_content)
            msg_for_arnel = '{}\n\nSender E-mail: {}\nSender Name: {} {}'.format(message, email, first_name, last_name)
            #try:
                # Send to Admin
                
            #send_mail(subject, msg_for_arnel, default_sender, [receiver], fail_silently=False)
                # mail.attach(attachment.name, attachment.read(), attachment.content_type)
                #mail.send()

                # Send copy to sender
            sub_for_anon_sender = "Thanks from https://www.arnelimperial.com"
            msg_for_anon_sender = "Hi {} {} \n{}\n\n\nI have received your message and would like to thank you for taking the time writing to me.\n\nI will reply by email as soon as possible.".format(first_name, last_name,email)
            # msg_anon_sender = EmailMultiAlternatives(sub_for_anon_sender, text_content, default_sender, [email])
            # msg_anon_sender.attach_alternative(html_content, "text/html")
            # msg_anon_sender.send()
            datatuple = (
            (subject, msg_for_arnel, default_sender, [arnel]),
            (sub_for_anon_sender, msg_for_anon_sender, default_sender, [email]),
            )
            #send_mass_mail(datatuple)
            send_mail(subject, message, default_sender, [arnel], fail_silently=False)

            return Response({"success": "Message was sent."}, status=status.HTTP_200_OK)
            #except:
                #raise APIException("Your message won't send. Refresh the page and try again.")
            
        return Response({"fail": "Failed! Message wont't sent"}, status=status.HTTP_502_BAD_GATEWAY)

