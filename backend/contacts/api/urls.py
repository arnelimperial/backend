from django.urls import path
from . views import ContactsAPIView

app_name = "contacts"
urlpatterns = [
    path("", ContactsAPIView.as_view()),
]
