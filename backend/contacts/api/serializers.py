from rest_framework import serializers
from django.core.validators import MinLengthValidator

class ContactsSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=30)
    email = serializers.EmailField()
    subject = serializers.CharField(
        validators=[MinLengthValidator(limit_value=5, message='Minimum char length of 5')],
        max_length=80)
    message = serializers.CharField()
    #attachment = serializers.FileField(allow_empty_file=True)

