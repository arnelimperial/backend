from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
#from django.template.defaultfilters import slugify
from django.utils.text import slugify
from django.urls import reverse
from markdown_deux import markdown

User = getattr(settings, 'AUTH_USER_MODEL')

class Blog(models.Model):
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='blogs',
        related_query_name='blog'
    )
    title = models.CharField(max_length=240)
    slug = models.SlugField(max_length=255, unique=True, editable=False)
    description = models.CharField(max_length=240, blank=True)
    photo = models.FileField(upload_to='blogs', blank=True)
    content = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-updated']
        unique_together = ('title', 'slug')

    def __str__(self):
        return self.title

    
    def get_absolute_url(self):
        return reverse('blogs:detail', kwargs={'slug': self.slug})
    
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)

    def get_markdown(self):
        content = self.content
        return markdown(content)
