from blogs.views import (
    blog_list_view,
    blog_detail_view,
    blog_create_view,
    blog_update_view,
    blog_delete_view
)
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = "blogs"
urlpatterns = [
    path('', view=blog_list_view, name='list'),
    path('<slug:slug>', view=blog_detail_view, name='detail'),
    path('create/', view=blog_create_view, name='create'),
    path('<slug:slug>/update/', view=blog_update_view, name='update'),
    path('<slug:slug>/delete/', view=blog_delete_view, name='delete'),
]