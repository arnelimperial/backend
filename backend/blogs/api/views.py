from rest_framework import viewsets, generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
# from rest_framework.decorators import authentication_classes, permission_classes
from backend.blogs.api.serializers import BlogSerializer
from backend.blogs.models import Blog
from .permissions import ReadOnly, IsAdminOrReadOnly, IsAuthorOrReadOnly

class BlogViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication,TokenAuthentication,]
    serializer_class = BlogSerializer

    permission_classes = [IsAdminOrReadOnly]

    queryset = Blog.objects.all()
    lookup_field = 'slug'

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
