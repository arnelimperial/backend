from rest_framework import serializers

from backend.blogs.models import Blog

class BlogSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField(read_only=True)
    created = serializers.DateTimeField(format="%B %d, %Y", read_only=True)
    updated = serializers.DateTimeField(format="%B %d, %Y", read_only=True)

    
    
    class Meta:
        model = Blog
        fields = ['author', 'title', 'slug', 'description', 'content', 'photo','created', 'updated', 'url']
       
        extra_kwargs = {
            "url": {"view_name": "api:blog-detail", "lookup_field": "slug"}
        }
