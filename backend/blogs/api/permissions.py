from rest_framework import permissions

from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model

User = get_user_model()
coder = User.objects.filter(is_staff=True)


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class IsAdminOrReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            return request.user.is_staff

# class IsAdminOrReadOnly(BasePermission):
#     def is_authenticated(self, request):
#         return request.user and is_authenticated(request.user)
    
#     def has_permission(self, request, view):
#         if view.action == 'list':
#             return True
#         if is_authenticated(request) and view.action == 'create' and request.user == coder:
#             return True
#         if is_authenticated(request) and view.action == 'update' and request.user == coder:
#             return True
        
#         return False

class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.author == request.user