from django.shortcuts import render
from backend.blogs.models import Blog
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404

class BlogListView(LoginRequiredMixin,ListView):
    model = Blog
    template_name = 'blogs/blog_list.html'
    ordering = ['-updated']
    blogs = Blog.objects.all()
    context_object_name = 'blogs'

    def get_context_data(self, *args, **kwargs):
        context = super(BlogListView, self).get_context_data(*args, **kwargs)
        context['title'] = 'New Post'
        context['description'] = 'Awesome new post'
        return context

blog_list_view = BlogListView.as_view()

class BlogDetailView(LoginRequiredMixin,DetailView):
    model = Blog
    template_name = 'blogs/blog_detail.html'
    

blog_detail_view = BlogDetailView.as_view()


class BlogCreateView(LoginRequiredMixin,CreateView):
    model = Blog
    template_name = 'blogs/blog_form.html'
    fields = ['title', 'description', 'content', 'photo',]

    def form_valid(self, form):
        form.instance.author = self.request.user
        #return super().form_valid(form)
        return super(BlogCreateView, self).form_valid(form)

blog_create_view = BlogCreateView.as_view()

class BlogUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Blog
    template_name = 'blogs/blog_form.html'
    fields = ['title', 'description', 'content','photo',]

    def form_valid(self, form):
        form.instance.author = self.request.user
        #return super().form_valid(form)
        return super(BlogUpdateView, self).form_valid(form)

    def test_func(self):
        blog = self.get_object()
        if blog.author == self.request.user:
            return True
        return False

blog_update_view = BlogUpdateView.as_view()


class BlogDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Blog
    template_name = 'blogs/blog_confirm_delete.html'
    success_url = reverse_lazy('blogs:list')
    
    def test_func(self):
        blog = self.get_object()
        if blog.author == self.request.user:
            return True
        return False
blog_delete_view = BlogDeleteView.as_view()