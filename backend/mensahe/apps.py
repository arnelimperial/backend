from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class MensaheConfig(AppConfig):
    name = 'backend.mensahe'
    verbose_name = _("Mensahe")

    def ready(self):
        try:
            import backend.mensahe.signals  # noqa F401
        except ImportError:
            pass

