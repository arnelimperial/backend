from django.shortcuts import render
from .models import Messages
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
#from .tasks import sleepy, send_to_sender


class MessagesListView(ListView):
    model = Messages
    template_name = 'mensahe/mensahe_list.html'
    ordering = ['-created']
    mensahe = Messages.objects.all()
    context_object_name = 'mensahes'

    def get_context_data(self, *args, **kwargs):
        context = super(MessagesListView, self).get_context_data(*args, **kwargs)
        #context['headline'] = 'New Post'
        #context['description'] = 'Awesome new post'
        return context

messages_list_view = MessagesListView.as_view()

class MessagesDetailView(DetailView):
    model = Messages
    template_name = 'mensahe/mensahe_detail.html'
    

messages_detail_view = MessagesDetailView.as_view()

class MessagesCreateView(LoginRequiredMixin,CreateView):
    model = Messages
    template_name = 'mensahe/mensahe_form.html'
    fields = ['first_name', 'last_name', 'subject', 'email', 'message',]

    # def form_valid(self, form):
    #     form.instance.user = self.request.user
    #     return super().form_valid(form)

messages_create_view = MessagesCreateView.as_view()

# def indexView(request):
#     sleepy.delay(10)
#     send_to_sender()
#     return HttpResponse('<h1>Message Sent</h1>')