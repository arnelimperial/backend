from .views import (
    messages_list_view,
    messages_detail_view,
    messages_create_view,
    #indexView,
)
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'mensahe'
urlpatterns = [
    path('', view=messages_list_view, name='list'),
    #path('index/', indexView, name='index'),
    #path('<slug:slug>', view=messages_detail_view, name='detail'),
    path('create/', view=messages_create_view, name='create'),
   
]