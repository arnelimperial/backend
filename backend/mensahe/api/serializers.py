from rest_framework import serializers
from backend.mensahe.models import Messages


class MessagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messages
        fields = ["id", "first_name", "last_name", "subject", "email", "message", "created",]

       