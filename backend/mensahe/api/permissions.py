from rest_framework import permissions

from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from django.contrib.auth import get_user_model

User = get_user_model()
coder = User.objects.filter(is_staff=True)

class PostOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        if self.action in ('create',):
            return True
        return False


class PostOnlyPermission(BasePermission):
    
    def has_permission(self, request, view):
        user = request.user
        if request.method == 'POST':
            return True



class IsAdminPermission(permissions.IsAdminUser):
    
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
    

