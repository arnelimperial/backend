from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAdminUser
from rest_framework.response import Response
from .serializers import MessagesSerializer
from .permissions import PostOnlyPermission, IsAdminPermission
from backend.mensahe.models import Messages

from django.http import Http404, HttpResponse
from rest_framework import status
from rest_framework.exceptions import APIException
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from datetime import timedelta, datetime


arnel = getattr(settings, 'EMAIL_RECEIVER')
default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')

# @method_decorator(csrf_exempt, name='dispatch')
class MessagesViewSet(ModelViewSet):
    serializer_class = MessagesSerializer
    queryset = Messages.objects.all()
    authentication_classes = []
    permission_classes = [PostOnlyPermission]

    # def get_permissions(self):
    #     if self.action == "create":
    #         permission_classes = [AllowAny,]

    #     else:
    #         permission_classes = [IsAdminUser]
        
    #     return [permission() for permission in self.permission_classes]

    #     #return super(self.__class__, self).get_permissions()

   
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

