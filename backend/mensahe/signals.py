from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta, datetime
from .models import Messages
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings


arnel = getattr(settings, 'EMAIL_RECEIVER')
default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')

@receiver(post_save, sender=Messages)
def send_mail_on_create(sender, instance=None, created=False, **kwargs):
    if created:
        obj = Messages.objects.all().first()
        
        msg_for_arnel = '{}\n\n\nSender E-mail: {}\nSender Name: {} {}'.format(
            obj.message, 
            obj.email,
            obj.first_name, 
            obj.last_name
        )
        send_mail(obj.subject, msg_for_arnel, default_sender, [arnel], fail_silently=False)


@receiver(post_save, sender=Messages)
def send_to_sender_on_create(sender, instance=None, created=False, **kwargs):
    if created:
    
        obj = Messages.objects.all().first()
        arnel = getattr(settings, 'EMAIL_RECEIVER')
        default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')
        html_content = render_to_string('mensahe/copy.html', {
            'first_name':obj.first_name, 
            'last_name':obj.last_name, 
            'email':obj.email, 
            'subject':obj.subject,
            'message':obj.message
            })

        text_content = strip_tags(html_content)
        sub_for_anon_sender = "Thanks from https://www.arnelimperial.com"
        msg_anon_sender = EmailMultiAlternatives(sub_for_anon_sender, text_content, default_sender, [obj.email])
        msg_anon_sender.attach_alternative(html_content, "text/html")
        msg_anon_sender.send()
        #return None


        