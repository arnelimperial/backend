from django.db import models
from django.core.validators import EmailValidator, MinLengthValidator, validate_email
from django.utils import timezone


class Messages(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    subject = models.CharField(
        validators=[
            MinLengthValidator(
                limit_value=4,
                message='Minimun of 4 characters'
            )],max_length=100
    )
    email = models.EmailField(validators=[validate_email])
    message = models.TextField()
    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-created']
    
    def __str__(self):
        return '{} {} - {}'.format(self.first_name, self.last_name, self.email)


