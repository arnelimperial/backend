from celery import shared_task
from celery import Celery
from config import celery
import os
from time import sleep
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from .models import Messages

# broker = 'redis://h:pbf42819619b7a375a90f8aa1e00ae31ff882be4a820cb3352f9ef067e536d4ad@ec2-34-234-176-9.compute-1.amazonaws.com:19929'
# celery = Celery('tasks', broker=broker)
# os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings.production"

# @shared_task
# def sleepy(duration):
#     sleep(duration)
#     return None

# @shared_task
# def send_to_sender():
#     sleep(10)
#     obj = Messages.objects.all().first()
#     arnel = getattr(settings, 'EMAIL_RECEIVER')
#     default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')
#     html_content = render_to_string('mensahe/copy.html', {
#         'first_name':obj.first_name, 
#         'last_name':obj.last_name, 
#         'email':obj.email, 
#         'subject':obj.subject,
#         'message':obj.message
#         })

#     text_content = strip_tags(html_content)
#     sub_for_anon_sender = "Thanks from https://www.arnelimperial.com"
#     msg_anon_sender = EmailMultiAlternatives(sub_for_anon_sender, text_content, default_sender, [obj.email])
#     msg_anon_sender.attach_alternative(html_content, "text/html")
#     msg_anon_sender.send()
#     return None

